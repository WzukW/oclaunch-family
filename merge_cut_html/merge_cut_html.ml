(* ©  Clément Joly, (2016)

   leowzukw@vmail.me

   This software is a computer program whose purpose is to merge bunch of html
   documents into one, optionaly removing some parts.

   This software is governed by the CeCILL-B license under French law and
   abiding by the rules of distribution of free software.  You can  use,
   modify and/ or redistribute the software under the terms of the CeCILL-B
   license as circulated by CEA, CNRS and INRIA at the following URL
   "http://www.cecill.info".

   As a counterpart to the access to the source code and  rights to copy,
   modify and redistribute granted by the license, users are provided only
   with a limited warranty  and the software's author,  the holder of the
   economic rights,  and the successive licensors  have only  limited
   liability.

   In this respect, the user's attention is drawn to the risks associated
   with loading,  using,  modifying and/or developing or reproducing the
   software by the user in light of its specific status of free software,
   that may mean  that it is complicated to manipulate,  and  that  also
   therefore means  that it is reserved for developers  and  experienced
   professionals having in-depth computer knowledge. Users are therefore
   encouraged to load and test the software's suitability as regards their
   requirements in conditions enabling the security of their systems and/or
   data to be ensured and,  more generally, to use and operate it in the
   same conditions as regards security.

   The fact that you are presently reading this means that you have had
   knowledge of the CeCILL-B license and that you accept its terms. *)


open Core.Std;;
open Soup;;

(* Group several elements of an HTML document together. We work the imperative
 * way, since lambdasoup seems to do it too. *)
class document path =
  (* Id can't have space, cache this to avoid recomputation. *)
  let normalised_name =
    String.filter path ~f:(function ' ' -> false | _ -> true)
  in
  object
    val name : string = path (* Creates problem if path is ~/doc.html of ./doc.html *)
    val soup = read_file path |> parse

    method name = name
    method s = soup
    method id = normalised_name

  end
;;

(* Merge two html documents, the second being appended to the first *)
let merge doc1 doc2 =
  append_child (doc1#s $ "body") (doc2#s $ "body");
  (* Wrap doc2 in article tag, adding id for the document name *)
  doc1#s $ "body > body" |> set_name "article";
  doc1#s $$ "body > article" |> last |> require
  |> set_attribute "id" doc2#id;
  doc1
;;

(* Generate empty document, with defined value for some head fields *)
let empty_document ?title ?lang ?charset ?(css=[]) ?(js=[]) () =
  let default_charset = Option.value charset ~default:"utf-8" in
  let add_attribute doc =
    let add_if_any ~f = function
      | None -> ()
      | Some value -> f value
    in
    add_if_any title ~f:(fun doc_title ->
      (create_element "title" ~inner_text:doc_title)
      |> append_child (doc $ "head"));
    add_if_any lang ~f:(fun doc_lang ->
      doc $ "html" |> set_attribute "lang" doc_lang);
    add_if_any charset ~f:(fun doc_encoding ->
      (create_element "meta" ~attributes:[("charset", doc_encoding)])
      |> append_child (doc $ "head"));
    List.iter css ~f:(fun path ->
      let style =
        create_element "link"
          ~attributes:[("rel", "stylesheet") ; ("href", path)
                      ; ("type", "text/css")
                      ; ("charset", default_charset)]
      in
      append_child (doc $ "head") style);
    List.iter js ~f:(fun path ->
      let script =
        create_element "script"
          ~attributes:[ ("src", path)
                      ; ("type", "text/javascript")
                      ; ("charset", default_charset)]
      in
      doc $ "head"
      |> append_child script);
  in
  let empty =
    "<!DOCTYPE html><html><head></head><body></body>"
    |> parse
  in
  add_attribute empty;
  ((object
    (* We use an exception since everuthing is merge in the empty document *)
    val name = lazy (failwith "No name for the empty document")
    val soup = empty

    method name = Lazy.force name
    method s = soup
    method id = Lazy.force name
  end) : document)
;;

(* Remove parts of an HTML document *)
let remove_parts selectors document =
  List.iter selectors ~f:(function
    | "" -> () (* Ignore empty selectors *)
    | selector -> document#s $$ selector |> iter delete)
;;

let spec =
  let open Command.Spec in
  empty
  +> flag "-title" (optional string)
       ~doc:"MyTitle Define title of the final document"
  +> flag "-lang" (optional string)
       ~doc:"en Define lang of the final document"
  +> flag "-charset" (optional string)
       ~doc:"utf-8 Define charset of the final document"
  +> flag "-js" (listed string)
       ~doc:"file Add given script to final document"
  +> flag "-css" (listed string)
       ~doc:"file Add given CSS stylesheet to final document"
  +> flag "-rm" (listed string)
       ~doc:"selector Remove element matching the given CSS selector \
             (maybe used multiple time)"
  +> anon (sequence ("HTML document" %: file))
;;

let readme () =
  "\
    This program helps you to merge a bunch of HTML documents. It may remove \
    part of them and add css stylesheet of your choice to the merged document.\n\
    It works great with pandoc, for instance to produce epub, using:\n\
    padoc -f html -t epub -i - -o your_output_file.epub\
  "
;;

let command =
  Command.basic spec ~readme
    ~summary:"Merge and remove parts of a list of HTML documents. \
              Result is given on stdout."
    (fun title lang charset css js to_remove files () ->
       let soups =
         List.map files ~f:(fun file -> new document file)
       in
       let selectors = to_remove in
       List.iter ~f:(remove_parts selectors) soups;
       let final_document =
         let empty_doc =
           empty_document ?title ?lang ?charset ~css ~js ()
         in
         List.fold soups ~init:(empty_doc) ~f:merge
       in
       to_string final_document#s |> write_channel stdout
    );;

let () =  Command.run command;;


