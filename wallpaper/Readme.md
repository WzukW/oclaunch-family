# Get a changing, high-resolution wallpaper

This script is a simple way to change your wallpaper as often as you want with
the always growing Unsplash collection.

Simple script designed to change your wallpaper to a random, featured unsplash
one. All Unsplash photos are published under CC0, so don’t worry!

## Give it a try

``` sh
$ curl -fsSL https://lnch.ml/wp | sh
```

### Dependancies

 + [feh](http://feh.finalrewind.org/)
 + [curl](https://curl.haxx.se/)
 + And of course, an internet connection
 
## Parameters

### Change source of image

The default source of image is in the script, but you may change it to any url
referring to an image directly.

For instance, do
``` sh
export UNSPLASH_SRC='https://source.unsplash.com/category/nature'
```
before you run the script.

You may find ideas to change the source of your photos at
https://source.unsplash.com/.

## License

```
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
```

