((name "backup_name")
(source "example_conf.scm
_build zbck.ml")
(target "/tmp/bck")
(tar_opt "-h -v")
(zbck_opt --non-encrypted)
(zbgc_opt fast))
