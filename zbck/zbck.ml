(*
©  Clément Joly, (2016)

leowzukw@oclaunch.eu.org

This software is a computer program whose purpose is to use ZBackup in a simpler
way (with profil and short command line).

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*)

open Core.Std;;

(* For gc command, meaning fast or deep *)
type fd = Fast | Deep with sexp;;
(* Main settings, ~s stands for this in functions below *)
type settings =
  {
    (* General settings *)
    name : string;
    source : string; (* File/folder to backup *)
    target : string; (* Folder to store backup *)
    (* Name of the file to be placed to ignore some path *)
    ignore_tag : string with default("NOBACKUP");

    (* Options of commands *)
    (* Option passed to tar *)
    tar_opt : string with default("-h"), sexp_drop_default;
    (* Option for ZBackup (named tags by `zbackup -h` )
     * --non-encrypted is the only thing we can guess *)
    zbck_opt : string with default("--non-encrypted"), sexp_drop_default;
    (* Special option for gc subcommand *)
    zbgc_opt : fd with default(Fast), sexp_drop_default;
  }
  with sexp (* Generate settings_of_sexp and sexp_of_settings functions *)
;;

(* Create string from fd type *)
let fd2str = function
  | Fast -> "fast"
  | Deep -> "deep"

(* Some constants, attached to ZBackup behavior *)
(* ZBackup folder *)
let zb_bck_folder = "backups";;

(* Helper functions, for the script *)
(* Show messages of the script differently *)
let print msg =
  msg
  |> (^) "==% "
  |> printf "%s%!\n"
;;
(* Test path (in fact a string of file name, separated by ' ') is valid before
 * using it.
 * create argument can be used to crate a folder if it does not exists
 * XXX This methode would lead to problems if a file contains ' ' itself *)
let path ?(create=false) paths =
  String.split ~on:' ' paths
  |> List.iter ~f:(fun p ->
    if (Sys.file_exists p |> function `Yes -> true | _ -> false)
    then ()
    else
      begin
        sprintf "Path '%s' does not exist." p |> print;
        if create
        then begin print "Creating it"; Unix.mkdir p ~perm:0o775 end
        else begin print "Exiting"; exit 2 end
      end
  );
  paths
;;
(* Validate settings (paths,…) *)
let validate ~s =
  (* Normalise path name, to avoid line return, leading to tar error *)
  let normalise = String.map ~f:(function '\n' -> ' ' | a -> a) in
  (* Create copy, normalised version. ns stands for normalised s *)
  let ns =
    { s with
    source = normalise s.source;
    target = normalise s.target
    }
  in
  path ns.source |> ignore;
  path ~create:true ns.target |> ignore;

  ns
;;
(* Gives path of backups *)
let backup_path ~s =
  String.concat [ s.target ; "/" ; zb_bck_folder; "/" ]
;;
(* Create backup name *)
let gen_name ~s =
  let now = (* To be inserted in backup name *)
    let t = Unix.time () in
    let l = t |> Unix.localtime in
    (* ISO8601 time: 2016-03-19T19:33:18, followed by unix time *)
    sprintf "%i-%i-%iT%i:%i:%i_%.2f"
      (l.Unix.tm_year + 1900)
      l.Unix.tm_mon
      l.Unix.tm_mday
      l.Unix.tm_hour
      l.Unix.tm_min
      l.Unix.tm_sec
      t (* Unix time *)
  in
  String.map s.source ~f:(function
   | ' ' | '/' -> '_'
   | c -> c)
  |> fun src -> String.concat
   [ (backup_path ~s) ; "bck_" ; now ; "_" ; src ; "-" ]
   (* Final - to limit problem with extensions (.sh for instance) *)
;;

(* Some general constants *)
let tar = "tar";; (* You may change it to use with another command (zip…) *)
(* Some constants, attached to ZBackup command-line interface *)
let zbackup = "zbackup";; (* ZBackup command *)
let init = "init";;
let backup = "backup";;
let restore = "restore";;
let gc = "gc";;
(* Helper function, to insert " " between arguments *)
let h_cmd li =
  List.intersperse ~sep:" " li
  |> String.concat
;;
(* Some subcommands *)
let init_cmd ~s = h_cmd [ zbackup ; init ; s.zbck_opt ; s.target ];;
let bck_cmd ~s = h_cmd [ zbackup ; backup ; s.zbck_opt ];;
let rest_cmd ~s backup_name target =
  h_cmd [ zbackup ; restore ; s.zbck_opt ; backup_name ;  " > " ; target ]
;;
let gc_cmd ~s =
  h_cmd [ zbackup ; gc ; s.zbgc_opt  |> fd2str ; s.zbck_opt ; s.target ]
;;

(* Exception raised when exit satus of the last command is different of 0 *)
exception Exit_with of int;;
(* Set of function to run ZBackup commands. An exception is fine, since we want
 * most of the time we want to stop at the first error *)
let run_zbk_exn cmd =
  sprintf "Running '%s'\n" cmd |> print;
  let start_time = Time.now () in
  Sys.command cmd
  |> (fun exit_code ->
      match exit_code with
      | 0 -> ()
      | _ -> raise (Exit_with exit_code));

  sprintf "Duration: %.3f seconds." Time.(diff (now ()) start_time |> Span.to_float) |> print
;;
let zb_init ~s =
  let open Command in
  basic ~summary:"Init backup repository from your profile settings."
  Spec.empty
  (fun () ->
  init_cmd ~s (*^ " " ^ (path Sys.argv.(2))*)
  |> run_zbk_exn
  )
;;

let zb_backup ~s =
  let open Command in
  basic ~summary:"Do a backup with your profile settings."
  Spec.(empty
  +> flag "--fast" no_arg
    ~doc:"Do not verify backup after doing it."
    ~aliases:["-f"])
  (fun fast () ->
    (* Tar command, concatenated to zbackup command *)
    let tar_cmd =
      String.concat
        [ tar ; " -c " ; s.tar_opt ; " --exclude-tag=" ; s.ignore_tag ; " " ;
        s.source ]
    in
    let backup_name = gen_name ~s in
    sprintf "Backup name: %s" backup_name |> print;
    (* Zbackup command, with backup name at the end *)
    let zbck = (bck_cmd ~s) ^ " " ^ backup_name in
    sprintf "%s | %s" tar_cmd zbck
    |> run_zbk_exn |> fun () -> print "Backup finished!";

    (* Verify backup
     * Since a checksum of the archive is computed, extracting it back will
     * verify it. *)
   (* According to the manual:
     * Furthermore, each backup is protected by its SHA256 sum, which is
     * calculated before piping the data into the deduplication logic. When  a
     * backup  is being restored, its SHA256 is calculated again and compared
     * against the stored one.  The program would fail on a mismatch.
     * Therefore, to ensure safety it is enough to restore each backup to
     * /dev/null immediately after creating it.  If it restores fine, it will
     * restore fine ever after.
    *)
    if not(fast)
    then begin
    print "Verifying backup. To avoid it, use --fast flag.";
    rest_cmd ~s backup_name "/dev/null"
    |> run_zbk_exn
    |> fun exn -> match exn with
      | exception Exit_with e -> sprintf "Backup failed (error code %i)" e
        |> print; exit 4
      | () -> ()
    end
    else print "No verification, --fast tag seen."
  )
;;

let zb_restore ~s =
  let open Command in
  basic ~summary:"Restore a backup with your profile settings. Therefore, just
  give the path after <backup-repo>/backups/ and the destination."
  Spec.(empty
  +> anon ("backup_name" %: string)
  +> anon ("destination" %: string))
  (fun name dest () ->
    rest_cmd ~s ((backup_path ~s) ^ name) dest
    |> run_zbk_exn
  )
;;

let zb_gc ~s =
  let open Command in
  basic ~summary:"Perform garbage collection."
  Spec.empty
  (fun () ->
    gc_cmd ~s
    |> run_zbk_exn
  )
;;

let list_backups ~s =
  let open Command in
  basic ~summary:"List your backups."
  Spec.empty
  (fun () ->
    print "Your backups:";
    Sys.ls_dir (backup_path ~s)
    |> List.iter ~f:(fun name ->
      print ("- " ^ name))
  )
;;

let beautify ~s =
  let open Command in
  basic ~summary:"Show a human readable profile."
  Spec.empty
  (fun () ->
    s
    |> sexp_of_settings
    |> Sexp.to_string_hum
    |> print_endline
  )
;;

let () =
  (* Read config and parse command-line arguments *)
  let s =
    Sexp.input_sexp In_channel.stdin
    |> settings_of_sexp
    |> (fun s -> validate ~s)
  in
  let open Command in
    group ~summary:"Wrapper script around ZBackup (http://zbackup.org/)."
    [ "init", zb_init ~s ; "backup", zb_backup ~s ; "restore", zb_restore ~s ;
    "gc", zb_gc ~s ; "list", list_backups ~s ; "humanise", beautify ~s ]
    |> run
;;
